\contentsline {chapter}{\fontsize {14pt}{\baselineskip }\selectfont {摘要}}{i}{}%
\contentsline {chapter}{\fontsize {14pt}{\baselineskip }\selectfont {目錄}}{ii}{}%
\contentsline {chapter}{\fontsize {14pt}{\baselineskip }\selectfont {圖目錄}}{iv}{}%
\contentsline {chapter}{\fontsize {14pt}{\baselineskip }\selectfont {表目錄}}{vi}{}%
\contentsline {chapter}{\numberline {1}簡介}{1}{}%
\contentsline {section}{\numberline {1.1}研究背景與動機}{1}{}%
\contentsline {section}{\numberline {1.2}研究目的與方法}{1}{}%
\contentsline {chapter}{\numberline {2}文獻探討}{2}{}%
\contentsline {section}{\numberline {2.1}平面機構分析套件}{2}{}%
\contentsline {subsection}{\numberline {2.1.1}Linkage}{2}{}%
\contentsline {subsection}{\numberline {2.1.2}M.Sketch}{3}{}%
\contentsline {section}{\numberline {2.2}平面機構合成套件}{4}{}%
\contentsline {subsection}{\numberline {2.2.1}MeKin2D}{4}{}%
\contentsline {subsection}{\numberline {2.2.2}WinMecC}{5}{}%
\contentsline {subsection}{\numberline {2.2.3}GIM}{5}{}%
\contentsline {section}{\numberline {2.3}比較結果}{7}{}%
\contentsline {section}{\numberline {2.4}自行車避震機構研究}{8}{}%
\contentsline {subsection}{\numberline {2.4.1}Single-pivot suspension(單軸懸架)}{8}{}%
\contentsline {subsection}{\numberline {2.4.2}Linkage-driven Single-pivot suspension(連桿驅動之單樞軸懸架)}{8}{}%
\contentsline {subsection}{\numberline {2.4.3}High-Pivot Idler suspension(高樞轉惰輪懸架)}{9}{}%
\contentsline {subsection}{\numberline {2.4.4}Twin-link suspension(雙連桿懸掛)}{9}{}%
\contentsline {subsection}{\numberline {2.4.5}Horst-link suspension(霍斯特鏈懸掛)}{10}{}%
\contentsline {chapter}{\numberline {3}Pyslvs-UI 套件介紹}{11}{}%
\contentsline {section}{\numberline {3.1}Pyslvs-UI 架構與原理}{11}{}%
\contentsline {subsection}{\numberline {3.1.1}平面連桿機構模擬}{11}{}%
\contentsline {subsection}{\numberline {3.1.2}平面連桿機構合成}{12}{}%
\contentsline {subsection}{\numberline {3.1.3}圖形化使用者介面}{15}{}%
\contentsline {section}{\numberline {3.2}Pyslvs-UI 編譯}{15}{}%
\contentsline {subsection}{\numberline {3.2.1}Pyslvs大綱架構}{15}{}%
\contentsline {subsection}{\numberline {3.2.2}環境與安裝套件}{17}{}%
\contentsline {section}{\numberline {3.3}Pyslvs-UI 範例}{19}{}%
\contentsline {subsection}{\numberline {3.3.1}曲柄滑塊機構}{19}{}%
\contentsline {subsection}{\numberline {3.3.2}曲柄搖桿機構}{20}{}%
\contentsline {subsection}{\numberline {3.3.3}史蒂芬生機構}{21}{}%
\contentsline {subsection}{\numberline {3.3.4}瓦特氏運動機構}{22}{}%
\contentsline {subsection}{\numberline {3.3.5}二段式提球機構}{23}{}%
\contentsline {chapter}{\numberline {4}登山車避震機構}{24}{}%
\contentsline {section}{\numberline {4.1}避震機構合成}{24}{}%
\contentsline {subsection}{\numberline {4.1.1}差分進化演算法}{24}{}%
\contentsline {subsection}{\numberline {4.1.2}適應性函數}{26}{}%
\contentsline {section}{\numberline {4.2}避震機構評量}{27}{}%
\contentsline {subsection}{\numberline {4.2.1}Anti-squat}{27}{}%
\contentsline {subsection}{\numberline {4.2.2}Anti-rise}{28}{}%
\contentsline {subsection}{\numberline {4.2.3}Leverage Ratio}{29}{}%
\contentsline {section}{\numberline {4.3}避震機構分析範例}{30}{}%
\contentsline {subsection}{\numberline {4.3.1}3D零件自動生成}{35}{}%
\contentsline {chapter}{\numberline {5}CoppeliaSim}{38}{}%
\contentsline {section}{\numberline {5.1}xml匯入Coppeliasim}{38}{}%
\contentsline {subsection}{\numberline {5.1.1}使用xml的目的}{38}{}%
\contentsline {subsection}{\numberline {5.1.2}CoppeliaSim基本介紹}{38}{}%
\contentsline {subsection}{\numberline {5.1.3}xml樹狀結構}{41}{}%
\contentsline {section}{\numberline {5.2}CoppeliaSim遠端API}{43}{}%
\contentsline {subsection}{\numberline {5.2.1}遠端API功能介紹}{43}{}%
\contentsline {chapter}{\numberline {6}結論}{46}{}%
\contentsline {chapter}{\numberline {7}未來研究建議}{47}{}%
\contentsline {chapter}{\fontsize {14pt}{\baselineskip }\selectfont {附錄}}{48}{}%
\contentsline {section}{\fontsize {14pt}{\baselineskip }\selectfont {LaTex}}{48}{}%
\contentsline {chapter}{\fontsize {14pt}{\baselineskip }\selectfont {參考文獻}}{51}{}%
\contentsline {chapter}{\fontsize {14pt}{\baselineskip }\selectfont {誌謝}}{52}{}%
\contentsline {chapter}{\fontsize {14pt}{\baselineskip }\selectfont {作者簡介}}{53}{}%
\contentsfinish 
